const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const autoprefixer = require('autoprefixer')

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: ['webpack-hot-middleware/client', './lib/core/Bootstrap.js'],
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),

        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css' ,
        })
    ],

    module: {
        rules: [
            {
                test: /\.s?[ac]ss$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        query: {
                            importLoaders: 1,
                            modules: true,
                            localIdentName: '[local]__[hash:base64:5]'
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [ autoprefixer() ]
                        }
                    },
                    'sass-loader',
                ],
            },
        ],
    },
}