/**
 * Creates and returns a function
 *
 * @param handlers
 * @returns {function(*=, *=): *}
 */
export default function mapActions(handlers) {
    return (state, action) => {
        for (const [actionType, actionHandler] of Object.entries(handlers)) {
            if (action.type === actionType) {
                state = actionHandler(state, action.payload)
            }
        }

        return state
    }
}