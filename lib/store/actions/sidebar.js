
/**
 * Creates a new category.
 * @param {string} title
 */
const createNewCategory = (title) => {
    return {
        type: 'SIDEBAR_CATEGORY_ADDED',
        payload: {
            id: Math.random(),
            title: title,
            pages: []
        }
    }
};

/**
 * Creates a new page.
 *
 * @param {string} title
 * @param {number} categoryId
 */
const createNewPage = (title, categoryId) => {
    return {
        type: 'SIDEBAR_CATEGORY_PAGE_ADDED',
        payload: {
            id: Math.random(),
            title: title,
            categoryId: categoryId
        }
    }
};

/**
 * Changes title of a page.
 *
 * @param id
 * @param nextTitle
 */
const editPageTitle = (id, nextTitle) => {
    return {
        type: 'SIDEBAR_CATEGORY_PAGE_TITLE_CHANGED',
        payload: {
            id: id,
            title: nextTitle
        }
    }
};

/**
 * Removes page with specified ID.
 * @param id
 */
const removePage = (id) => {
    return {
        type: 'SIDEBAR_CATEGORY_PAGE_REMOVED',
        payload: {
            id: id
        }
    }
}

/**
 * Changes title of a category.
 *
 * @param {number} id
 * @param {string} title
 */
const editCategoryTitle = (id, title) => {
    return {
        type: 'SIDEBAR_CATEGORY_TITLE_CHANGED',
        payload: {
            id: id,
            title: title
        }
    }
};

/**
 * Removes category with specified ID.
 * @param {number} id
 */
const removeCategory = (id) => {
    return {
        type: 'SIDEBAR_CATEGORY_REMOVED',
        payload: {
            id: id
        }
    }
};

/**
 * Adds a notification.
 *
 * @param {string} text
 * @param {function} callback
 */
const pushNotification = (text, callback = function() {}) => {
    return {
        type: 'SIDEBAR_PUSH_NOTIFICATION',
        payload: {
            id: Math.random(),
            text: text,
            callback: callback
        }
    }
};

/**
 * Removes a notification with specified ID.
 * @param {number} id
 */
const popNotification = (id) => {
    return {
        type: 'SIDEBAR_POP_NOTIFICATION',
        payload: {
            id: id
        }
    }
};

const changeActivePageId = (id) => {
    return {
        type: 'SIDEBAR_ACTIVE_PAGE_CHANGED',
        payload: {
            id: id
        }
    }
}

const doCategoryTimeTravel = () => {
    return {
        type: 'SIDEBAR_CATEGORY_TIME_TRAVEL'
    }
};

const doCategoryPagesTimeTravel = (id) => {
    return {
        type: 'SIDEBAR_CATEGORY_PAGES_TIME_TRAVEL',
        payload: {
            categoryId: id
        }
    }
}

export default {
    createNewCategory, createNewPage, editPageTitle, removePage, editCategoryTitle, removeCategory,
    pushNotification, popNotification, changeActivePageId, doCategoryTimeTravel, doCategoryPagesTimeTravel
}