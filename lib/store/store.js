import { createStore } from 'redux'
import _ from 'lodash'

import sidebarReducer from '@store/reducers/sidebar'
import sidebarActions from '@store/actions/sidebar'

let initialStore = {
    activePageId: 0,
    sidebar: {
        notifications: [],
        categories: [],
        categoriesBackup: []
    },
}

let reducers = [
    sidebarReducer
];

const store = createStore((state, action) => {
    state = _.cloneDeep(state)

    for (let reducer of reducers) {
        state = reducer(state, action)
    }

    return state
}, initialStore)

/**
 * Wraps all functions inside actions to perform a dispatch on their result.
 * @param {object} actions
 */
const wrapActions = (actions) => {
    let result = {}

    for (const [name, func] of Object.entries(actions)) {
        result[name] = (...args) => {
            store.dispatch(func(...args))
        }
    }

    return result
}

export default {
    store: store,
    dispatch: store.dispatch,
    sidebar: wrapActions(sidebarActions)
}