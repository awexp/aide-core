import mapActions from '@store/mapActions'
import _ from 'lodash'

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const notificationPush = (state, payload) => {
    state.sidebar.notifications.push(payload)
    return state
};

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const notificationPop = (state, payload) => {
    state.sidebar.notifications.forEach((notification, idx, array) => {
        if (notification.id === payload.id) {
            array.splice(idx, 1)
        }
    })

    return state
};

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryAdded = (state, payload) => {
    state.sidebar.categories.push(payload)
    state.sidebar.categoriesBackup = _.cloneDeep(state.sidebar.categories)

    return state
};

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryTitleChanged = (state, payload) => {
    state.sidebar.categories.forEach((category) => {
        if (category.id === payload.id) {
            category.title = payload.title
        }
    })

    return state
}

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryRemoved = (state, payload) => {
    state.sidebar.categoriesBackup = _.cloneDeep(state.sidebar.categories)
    state.sidebar.categories.forEach((category, idx, array) => {
        if (category.id === payload.id) {
            array.splice(idx, 1)
        }
    })

    return state
};

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryPageAdded = (state, payload) => {
    state.sidebar.categories.forEach((category) => {
        if (category.id === payload.categoryId) {
            category.pages.push({
                id: payload.id,
                title: payload.title
            })
        }
    })

    return state
};

/**
 *
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryPageTitleChanged = (state, payload) => {
    state.sidebar.categories.forEach(category => {
        category.pages.forEach(page => {
            if (page.id === payload.id) {
                page.title = payload.title
            }
        })
    })

    return state
};

/**
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryPageRemoved = (state, payload) => {
    state.sidebar.categoriesBackup = _.cloneDeep(state.sidebar.categories)
    state.sidebar.categories.forEach(category => {
        category.pages.forEach((page, idx) => {
            if (page.id === payload.id) {
                category.pages.splice(idx, 1)
            }
        })
    })

    return state
};

/**
 * Brings back all categories that existed on previous store update.
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryTimeTravel = (state, payload) => {
    state.sidebar.categories = state.sidebar.categoriesBackup
    return state
};

/**
 * Brings back all category pages that existed on previous store update.
 *
 * @param state
 * @param payload
 * @return {object}
 */
const categoryPagesTimeTravel = (state, payload) => {
    // TODO: Bring back only certain pages, not all of them!
    state.sidebar.categories = state.sidebar.categoriesBackup

    return state
};

const activePageChanged = (state, payload) => {
    state.activePageId = payload.id
    return state
}

export default mapActions({
    'SIDEBAR_PUSH_NOTIFICATION': notificationPush,
    'SIDEBAR_POP_NOTIFICATION': notificationPop,

    'SIDEBAR_CATEGORY_ADDED': categoryAdded,
    'SIDEBAR_CATEGORY_REMOVED': categoryRemoved,
    'SIDEBAR_CATEGORY_TITLE_CHANGED': categoryTitleChanged,
    'SIDEBAR_CATEGORY_TIME_TRAVEL': categoryTimeTravel,

    'SIDEBAR_CATEGORY_PAGE_ADDED': categoryPageAdded,
    'SIDEBAR_CATEGORY_PAGE_TITLE_CHANGED': categoryPageTitleChanged,
    'SIDEBAR_CATEGORY_PAGE_REMOVED': categoryPageRemoved,
    'SIDEBAR_CATEGORY_PAGES_TIME_TRAVEL': categoryPagesTimeTravel,

    'SIDEBAR_ACTIVE_PAGE_CHANGED': activePageChanged
})
