import React from 'react'
import styles from '@styles/components/sidebar.scss'

export default class SidebarNewCategoryButton extends React.Component {
    render() {
        return (
            <div className={styles.newCategoryButton} onClick={this.props.onClick}>
                <span className={styles.newCategoryButton__icon}>
                    <i className="fas fa-plus" />
                </span>

                Add new category
            </div>
        )
    }
}