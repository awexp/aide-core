import React from 'react'
import styles from '@styles/components/sidebar.scss'
import store from '@store/store'

export default class SidebarNotification extends React.Component {
    state = {
        shouldPlayExitAnimation: false
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.notification.id !== this.props.notification.id) {
            this.setState({
                shouldPlayExitAnimation: false
            })

            this.setupExitAnimation()
        }
    }

    componentDidMount() {
        this.setupExitAnimation()
    }

    setupExitAnimation() {
        setTimeout(() => {
            this.setState({ shouldPlayExitAnimation: true })

            // Pop notification after animation ends.
            setTimeout(() => {
                store.sidebar.popNotification(this.props.notification.id)
            }, 300)
        }, 2500)
    }

    onTimeTravelClick = () => {
        this.props.notification.callback()
        this.setState({ shouldPlayExitAnimation: true })
    }

    render() {
        const activeAnimation = (
            this.state.shouldPlayExitAnimation ? styles['notification--play-exit-anim'] : styles['notification--play-enter-anim']
        )

        return (
            <div className={[styles.notification, activeAnimation].join(' ')}>
                <div className={styles.notification__text}>
                    {this.props.notification.text}
                </div>

                <div className={styles.notification__button} onClick={this.onTimeTravelClick}>
                    <i className="fas fa-angle-double-left" />
                </div>
            </div>
        )
    }
}