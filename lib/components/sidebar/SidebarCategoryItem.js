import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import store from '@store/store'

import styles from '@styles/components/sidebar.scss'
import SidebarCategoryNameEditForm from '@components/sidebar/SidebarCategoryNameEditForm'

export default class SidebarCategoryItem extends React.Component {
    state = {
        shouldShowNameEditForm: false
    }

    static propTypes = {
        item: PropTypes.object.isRequired
    }

    onEditTitleClick = () => {
        this.setState({
            shouldShowNameEditForm: true
        })
    }

    onEditTitleFormSubmit = (nextTitle) => {
        store.sidebar.editPageTitle(this.props.item.id, nextTitle)

        this.setState({
            shouldShowNameEditForm: false
        })
    }

    onEditTitleFormCancel = () => {
        this.setState({
            shouldShowNameEditForm: false
        })
    }

    onRemoveClick = () => {
        store.sidebar.removePage(this.props.item.id)
        store.sidebar.pushNotification('Page has been removed.', () => {
            store.sidebar.doCategoryPagesTimeTravel(0)
        })
    }

    onLinkClick = () => {
        store.sidebar.changeActivePageId(this.props.item.id)
    }

    renderItemName() {
        return (
            <div>
                <Link to='/category-1/new-page' onClick={this.onLinkClick}>{this.props.item.title}</Link>

                <div className={styles.category__actions} style={{marginRight: '-12px'}}>
                    <div className={styles.category__action} onClick={this.onEditTitleClick}>
                        <i className="fas fa-edit" />
                    </div>

                    <div className={styles.category__action} onClick={this.onRemoveClick}>
                        <i className="fas fa-trash" />
                    </div>

                    <div className={[styles.category__action, styles.category__action__move].join(' ')}>
                        <i className="fas fa-ellipsis-v" />
                    </div>
                </div>
            </div>
        )
    }

    renderItemNameEditForm() {
        return (
            <div>
                <SidebarCategoryNameEditForm name={this.props.item.title} onSubmit={this.onEditTitleFormSubmit} onCancel={this.onEditTitleFormCancel} />
            </div>
        )
    }

    render() {
        let statusClassName = this.props.isActive ? styles['category__item--active'] : ''

        return (
            <div className={[styles.category__item, statusClassName].join(' ')}>
                {this.state.shouldShowNameEditForm ? (
                    this.renderItemNameEditForm()
                ) : (
                    this.renderItemName()
                )}
            </div>
        )
    }
}