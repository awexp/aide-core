import React from 'react'
import PropTypes from 'prop-types'
import store from '@store/store'
import styles from '@styles/components/sidebar.scss'
import SidebarCategoryItem from '@components/sidebar/SidebarCategoryItem'
import SidebarCategoryNameEditForm from '@components/sidebar/SidebarCategoryNameEditForm'

export default class SidebarCategory extends React.Component {
    static propTypes = {
        category: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)
        this.state = this.mapPropsToState(props)
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState(this.mapPropsToState(nextProps))
    }

    /**
     * Initializes internal state based on given props.
     *
     * @param {object} props
     * @return {object}
     */
    mapPropsToState(props) {
        return {
            category: props.category,
            categoryTitle: props.category.title,
            pages: props.category.pages,
            shouldShowNameEditForm: false
        }
    }

    /**
     *
     * @param event
     */
    onAddPageActionClick = (event) => {
        store.sidebar.createNewPage('New Page', this.state.category.id)
    }

    /**
     *
     * @param event
     */
    onEditCategoryNameActionClick = (event) => {
        this.setState({
            shouldShowNameEditForm: true
        })
    }

    /**
     * Called when the user submits category name edit form.
     * @param {string} nextTitle
     */
    onNameEditFormSubmit = (nextTitle) => {
        store.sidebar.editCategoryTitle(this.state.category.id, nextTitle)
    }

    /**
     * Called when the user cancels changing the name of this category.
     */
    onNameEditFormCancel = () => {
        this.setState({
            shouldShowNameEditForm: false
        })
    }

    /**
     *
     * @param event
     */
    onRemoveCategoryActionClick = (event) => {
        store.sidebar.removeCategory(this.state.category.id)
        store.sidebar.pushNotification('Category has been removed.', () => {
            store.sidebar.doCategoryTimeTravel()
        })
    }

    renderCategoryNameEditForm() {
        return (
            <div>
                <SidebarCategoryNameEditForm name={this.state.categoryTitle} onSubmit={this.onNameEditFormSubmit} onCancel={this.onNameEditFormCancel} />
            </div>
        )
    }

    renderCategoryName() {
        return (
            <span>
                {this.state.categoryTitle}

                <div className={styles.category__actions}>
                    <div className={styles.category__action} onClick={this.onAddPageActionClick}>
                        <i className="fas fa-plus" />
                    </div>

                    <div className={styles.category__action} onClick={this.onEditCategoryNameActionClick}>
                        <i className="fas fa-edit" />
                    </div>

                    <div className={styles.category__action} onClick={this.onRemoveCategoryActionClick}>
                        <i className="fas fa-trash" />
                    </div>

                    <div className={[styles.category__action, styles.category__action__move].join(' ')}>
                        <i className="fas fa-ellipsis-v" />
                    </div>
                </div>
            </span>
        )
    }

    renderNoItemsMessage() {
        return (
            <div className={styles.category__noItemsMessage}>
                No pages.
            </div>
        )
    }

    renderItems() {
        return (
            <span>
                {this.state.pages.map((page, pageIdx) => (
                    <SidebarCategoryItem key={pageIdx} item={page} isActive={this.isItemActive(page)} />
                ))}
            </span>
        )
    }

    render() {
        return (
            <div className={styles.category}>
                <div className={styles.category__title}>
                    {this.state.shouldShowNameEditForm ? (
                        this.renderCategoryNameEditForm()
                    ) : (
                        this.renderCategoryName()
                    )}
                </div>
                <div className={styles.category__items}>
                    {this.state.pages.length === 0 ? (
                        this.renderNoItemsMessage()
                    ) : (
                        this.renderItems()
                    )}
                </div>
            </div>
        )
    }

    isItemActive = (item) => {
        return this.props.isItemActive(item)
    }
 }
