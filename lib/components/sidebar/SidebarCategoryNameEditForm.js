import React from 'react'
import styles from '@styles/components/sidebar.scss'

export default class SidebarCategoryNameEditForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: props.name
        }
    }

    render() {
        return (
            <span onKeyUp={this.onFormKeyPress}>
                <input type="text" value={this.state.name} onChange={this.onInputChange} />

                <div className={styles.category__actions}>
                    <div className={styles.category__action} onClick={this.onSubmit}>
                        <i className="fas fa-check" />
                    </div>

                    <div className={styles.category__action} onClick={this.onCancel}>
                        <i className="fas fa-times" />
                    </div>
                </div>
            </span>
        )
    }

    onInputChange = (event) => {
        this.setState({
            name: event.target.value
        })
    }

    onSubmit = () => {
        if (this.props.onSubmit) {
            this.props.onSubmit(this.state.name)
        }
    }

    onCancel = () => {
        if (this.props.onCancel) {
            this.props.onCancel()
        }
    }

    onFormKeyPress = (event) => {
        if (event.keyCode === 13) {
            this.onSubmit()
        }
    }
}