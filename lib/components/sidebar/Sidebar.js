import React from 'react'
import styles from '@styles/components/sidebar.scss'
import store from '@store/store'
import { connect } from 'react-redux'
import SidebarCategory from '@components/sidebar/SidebarCategory'
import SidebarNewCategoryButton from '@components/sidebar/SidebarNewCategoryButton'
import SidebarNotification from '@components/sidebar/SidebarNotification'

class Sidebar extends React.Component {
    constructor(props) {
        super(props)

        this.activeNotification = null
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.notifications.length > 0) {
            this.activeNotification = nextProps.notifications[0]
        } else {
            this.activeNotification = null
        }
    }

    onNewCategoryButtonClick = () => {
        store.sidebar.createNewCategory('Category ' + (this.props.categories.length + 1))
    }

    renderCategories() {
        if (this.props.categories.length === 0) {
            return (
                <div className={styles.noCategoriesMessage}>No categories.</div>
            )
        }

        return (
            <span>
                {this.props.categories.map(category => (
                    <SidebarCategory key={category.id} category={category} isItemActive={this.isItemActive} />
                ))}
            </span>
        )
    }

    render() {
        return (
            <div className={styles.sidebar}>
                {this.renderCategories()}

                <SidebarNewCategoryButton onClick={this.onNewCategoryButtonClick} />

                {this.activeNotification !== null ? (
                    <SidebarNotification notification={this.activeNotification} />
                ) : ''}
            </div>
        )
    }

    isItemActive = (item) => {
        return item.id === this.props.activePageId
    }
}

export default connect(state => {
    return {
        categories: state.sidebar.categories,
        notifications: state.sidebar.notifications,
        activePageId: state.activePageId,
    }
})(Sidebar)