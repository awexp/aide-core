import React from 'react'
import styles from '@styles/app.scss'
import PageElement from '@pages/PageElement'

export default class PageHeader extends PageElement {
    renderElement() {
        return (
            <h2>
                {this.metadata.title}
                <span className={styles.headingSubtitle}>{this.metadata.description}</span>
            </h2>
        )
    }

    getInitialMetadata() {
        return {
            title: '',
            description: ''
        }
    }
}