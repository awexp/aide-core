import React from 'react'

export default class Table extends React.Component {
    render() {
        return (
            <table>
                <thead>
                    <tr>
                        {this.props.tableHeader.map((entry, idx) => (
                            <th key={idx}>{entry}</th>
                        ))}
                    </tr>
                </thead>

                <tbody>
                    {this.props.tableEntries.map((entry, idx) => (
                        <tr key={idx}>
                            {entry.map((entry, idx) => (
                                <td key={idx}>{entry}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        )
    }
}