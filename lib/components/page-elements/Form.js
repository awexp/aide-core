import React from 'react'
import PageElement from '@pages/PageElement'

export default class Form extends PageElement {
    handleSubmit = (e) => {
        e.preventDefault()

        // APIRequest...
    }

    renderElement() {
        return (
            <form onSubmit={this.handleSubmit}>
                {this.metadata.inputs.map(input => (
                    <div key={Math.random()} style={{margin: '1rem 0'}}>
                        <input id={input.id} placeholder={input.placeholder} type={input.type} /><br />
                    </div>
                ))}

                <input type="submit" value="Submit" />
            </form>
        )
    }

    getInitialMetadata() {
        return {
            inputs: []
        }
    }
}