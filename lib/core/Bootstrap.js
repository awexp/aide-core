import * as React from 'react'
import * as ReactDOM from 'react-dom'

import AideApplication from './AideApplication'
import AideAPIBroker from './AideAPIBroker'
import Page from '@pages/Page'
import PageElement from '@pages/PageElement'
import PageExtension from '@pages/PageExtension'

class Bootstrap {
    static bootstrapApplication() {
        ReactDOM.render(
            <AideApplication />,
            document.getElementById('app')
        )
    }
}

// Start the application with a slight timeout to let the project initialize.
setTimeout(() => {
    Bootstrap.bootstrapApplication()
})

window.Aide = new AideAPIBroker()
window.AidePage = Page
window.AidePageExtension = PageExtension
window.AidePageElement = PageElement