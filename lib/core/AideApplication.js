import * as React from 'react'
import { hot } from 'react-hot-loader'
import { Provider } from 'react-redux'
import { HashRouter, Route } from 'react-router-dom'
import PageContainer from '@pages/PageContainer'
import store from '@store/store'
import styles from '@styles/app.scss'
import Sidebar from '@components/sidebar/Sidebar'

class AideApplication extends React.Component {
    render() {
        return (
            <Provider store={store.store}>
                <div className={styles.appContainer}>
                    <HashRouter>
                        <div>
                            <Sidebar />

                            <div className={[styles.pageContainer, styles.container].join(' ')}>
                                <Route path='/:category/:page' component={PageContainer} />
                            </div>
                        </div>
                    </HashRouter>
                </div>
            </Provider>
        )
    }
}

export default hot(module)(AideApplication)