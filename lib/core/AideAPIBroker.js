import PageElementsRegistry from '@pages/PageElementsRegistry'

export default class AideAPIBroker {
    static implementors = {}

    setupProject(project) {
        let pages = project.pageRegistry.items
        let extensions = project.pageExtensionsRegistry.items
        let elements = project.pageElementsRegistry.items

        for (let [pageName, page] of Object.entries(pages)) {
            AideAPIBroker.implementors[pageName] = {pageProto: page, extensions: []}
        }

        for (let [pageName, extensionProto] of Object.entries(extensions)) {
            let extensions = AideAPIBroker.implementors[pageName].extensions
            extensions.push(extensionProto)
        }

        for (let [elementName, componentClass] of Object.entries(elements)) {
            PageElementsRegistry.register(elementName, componentClass)
        }
    }

    static getPageImplementor(name) {
        return AideAPIBroker.implementors[name]
    }
}