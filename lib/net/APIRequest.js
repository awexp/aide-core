import requestify from 'requestify'

export default class APIRequest {
    static baseURL = 'http://aide-backend.test/'

    /**
     * Gets data from the server.
     *
     * @param url
     * @param params
     *
     * @return {Promise<any>}
     */
    static get(url, params) {
        if (url.indexOf('/') === 0) url = url.substr(1, url.length)
        url = APIRequest.baseURL + url

        return new Promise((resolve, reject) => {
            requestify.get(url, params).then(response => {
                response = JSON.parse(response.body)

                if (response.success) {
                    resolve(response)
                } else {
                    reject(response)
                }
            }).catch(err => {
                reject(err)
            })
        })
    }
}