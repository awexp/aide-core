export default class PageExtension {
    constructor(page) {
        this.page = page
    }

    load() { }

    findElementById(id) {
        return this.page.findElementById(id)
    }
}