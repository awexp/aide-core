import React from 'react'
import styles from '@styles/components/page-container.scss'
import AideAPIBroker from '@core/AideAPIBroker'
import PageExtension from '@pages/PageExtension'

export default class PageContainer extends React.Component {

    state = {
        isMounted: false,
        isLoadingPage: true,
        pageInstance: null,
        pageDetails: {
            type: ''
        },
    }

    constructor(props) {
        super(props)

        let routerParams = props.match.params
        this.fetchPageDetails(routerParams.category, routerParams.page)
    }

    componentWillReceiveProps(nextProps, nextContext) {
        let routerParams = nextProps.match.params
        this.fetchPageDetails(routerParams.category, routerParams.page)
    }

    componentDidMount() {
        this.setState({
            isMounted: true
        })
    }

    /**
     * Fetches all informations required to render the page.
     *
     * @param categoryTitle
     * @param pageTitle
     */
    fetchPageDetails(categoryTitle, pageTitle) {
        if (this.state.isMounted) {
            this.setState({ isLoadingPage: true })
        }

        setTimeout(() => {
            if (!this.state.isMounted) {
                return
            }

            let pageType = 'TEST_PAGE'
            let pageImplementor = AideAPIBroker.getPageImplementor(pageType)
            let pageClass = pageImplementor.pageProto
            let pageInstance = new pageClass()

            pageInstance.initializePage().then(() => {
                this.setState({
                    isLoadingPage: false,
                    pageInstance: pageInstance,
                    pageDetails: {
                        type: pageType
                    }
                })

                // Load extensions after the page's been rendered.
                setTimeout(() => {
                    let extensions = pageImplementor.extensions

                    for (let extensionProto of extensions) {
                        if (extensionProto.prototype.load) {
                            let extension = new extensionProto(pageInstance)
                            extension.load()
                        } else {
                            let extension = new PageExtension(pageInstance)
                            extensionProto.call(extension)
                        }
                    }
                })
            }).catch(err => {
                this.setState({
                    isLoadingPage: false
                })
            })
        }, 300)
    }

    renderLoader() {
        return (
            <div className={styles.pageLoader}>
                <i className="fas fa-circle-notch fa-spin" />
            </div>
        )
    }

    renderPage() {
        if (this.state.pageInstance === null) {
            return (
                <div className={styles.page}>
                    <h4 style={{textAlign: 'center'}}>An error occurred while rendering the page.</h4>
                </div>
            )
        }

        return (
            <div className={styles.page}>
                {this.state.pageInstance.render()}
            </div>
        )
    }

    render() {
        return (
            <div>
                {this.state.isLoadingPage ? (
                    this.renderLoader()
                ) : (
                    this.renderPage()
                )}
            </div>
        )
    }
}