import React from 'react'

export default class PageElement extends React.Component {
    state = {
        '__aide__metadata': {},
        '__aide__props': {}
    }

    mountCallbacks = []
    mounted = false

    constructor(props) {
        super(props)

        this.state['__aide__metadata'] = this.getInitialMetadata()
    }

    componentDidMount() {
        this.mounted = true

        this.mountCallbacks.forEach(func => func())
        this.mountCallbacks = []
    }

    /**
     * Specifies a function to be called after the component's been mounted.
     * @param {Function} func
     */
    addMountCallback(func) {
        this.mountCallbacks.push(func)
    }

    render() {
        return (
            <span {...this.state['__aide__props']}>
                {this.renderElement()}
            </span>
        )
    }

    renderElement() {}

    /**
     * Gets whether the component has been mounted.
     * @return {boolean}
     */
    isMounted() {
        return this.mounted
    }

    get metadata() {
        return this.state['__aide__metadata']
    }

    getInitialMetadata() {
        return {}
    }
}