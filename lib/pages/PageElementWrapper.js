export default class PageElementWrapper {
    constructor(elementPrototype) {
        this.elementPrototype = elementPrototype
        this.componentInstance = null
    }

    getId() {
        return this.elementPrototype.id
    }

    /**
     * @return {string}
     */
    getType() {
        return this.elementPrototype.type
    }

    /**
     *
     * @param {string} key
     * @param {object} nextState
     */
    updateComponentStateByKey(key, nextState) {
        const updateComponentState = () => {
            let metadata = this.componentInstance.state[key] || {}

            for (const [key, value] of Object.entries(nextState)) {
                metadata[key] = value
            }

            this.componentInstance.setState({
                [key]: metadata
            })
        }

        if (!this.componentInstance.isMounted()) {
            this.componentInstance.addMountCallback(updateComponentState)
        } else {
            updateComponentState()
        }
    }

    /**
     *
     * @param {object} nextState
     */
    setState(nextState) {
        const updateComponentState = () => {
            this.componentInstance.setState(nextState)
        }

        if (!this.componentInstance.isMounted()) {
            this.componentInstance.addMountCallback(updateComponentState)
        } else {
            updateComponentState()
        }
    }

    get state() {
        return this.componentInstance.state
    }

    /**
     * Updates element's metadata.
     * @param {object} nextMetadata
     */
    setMetadata(nextMetadata) {
        this.updateComponentStateByKey('__aide__metadata', nextMetadata)
    }

    get metadata() {
        return this.componentInstance.state['__aide__metadata']
    }

    setProps(nextProps) {
        this.updateComponentStateByKey('__aide__props', nextProps)
    }

    get props() {
        return this.componentInstance.props
    }

    /**
     * @param {PageElement} instance
     */
    setComponent(instance) {
        this.componentInstance = instance
    }

    /**
     * @return {PageElement}
     */
    getComponent() {
        return this.componentInstance
    }

    /**
     * @return {object}
     */
    getPrototypeMetadata() {
        return this.elementPrototype.metadata
    }
}