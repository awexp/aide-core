import PageHeader from '@components/page-elements/PageHeader'
import Table from '@components/page-elements/Table'
import Form from '@components/page-elements/Form'

export default class PageElementsRegistry {
    static registry = {
        'NATIVE_PAGE_HEADER': PageHeader,
        'NATIVE_TABLE': Table,
        'NATIVE_FORM': Form
    }

    /**
     *
     * @param {string} type
     * @param {React.Component} component
     */
    static register(type, component) {
        PageElementsRegistry.registry[type] = component
    }

    /**
     *
     * @param {string} type
     * @return {React.Component}
     */
    static findComponentByType(type) {
        return PageElementsRegistry.registry[type]
    }
}