import React from 'react'

import APIRequest from '../net/APIRequest'
import PageElementsRegistry from '@pages/PageElementsRegistry'
import PageElement from '@pages/PageElement'
import PageElementWrapper from '@pages/PageElementWrapper'

export default class Page extends React.Component {
    /**
     * @type {object}
     */
    page = {
        title: '',
        elements: []
    }

    /**
     * Holds references to rendered page elements.
     * @type {Array}
     */
    elementRefs = []

    /**
     * Specifies API endpoint that will be used to fetch details of the page.
     * @type {string}
     */
    endpoint = ''

    /**
     * Fetches details about how this page should be rendered from the server.
     * @return {Promise<any>}
     */
    initializePage() {
        return new Promise((resolve, reject) => {
            APIRequest.get(this.endpoint, this.getDetailsRequestParams()).then(response => {
                this.page.title = response.details.title
                this.page.elements = response.details.elements.map(element => {
                    return new PageElementWrapper(element)
                })

                resolve()
            }).catch(err => {
                console.error(err)
                reject()
            })
        })
    }

    /**
     * Renders the page.
     * @returns {*}
     */
    render() {
        return (
            <div>
                {this.page.elements.map((element, idx) => (
                    <React.Fragment key={idx}>{this.renderElement(element)}</React.Fragment>
                ))}
            </div>
        )
    }

    /**
     * Renders specified element.
     *
     * @param {PageElementWrapper} element
     * @returns {*}
     */
    renderElement(element) {
        let elementComponent = PageElementsRegistry.findComponentByType(element.getType())
        if (elementComponent === undefined) {
            console.error('Component for page element with type "' + element.getType() + '" was not found.')
            return (<React.Fragment />)
        }

        const refId = Math.random()
        this.elementRefs[refId] = React.createRef()

        // React requires elements to start with an uppercase letter to differentiate them
        // from standard HTML tags.
        const PageElementComponent = elementComponent

        // Set initial metadata of the element after it's been rendered.
        setTimeout(() => {
            element.setComponent(this.elementRefs[refId].current)
            element.setMetadata(element.getPrototypeMetadata())
            element.setState(element.getPrototypeMetadata())
        })

        return (
            <React.Fragment>
                <PageElementComponent ref={this.elementRefs[refId]} />
            </React.Fragment>
        )
    }

    /**
     * Finds element with specified identifier and returns its metadata.
     *
     * @param id
     * @return {object|null}
     */
    findElementMetadataById(id) {
        let element = this.page.elements.find(element => {
            return element.getId() === id
        })

        return element ? element.getMetadata() : null
    }

    /**
     * Find the elements with specified ID.
     *
     * @param id
     * @return {PageElement}
     */
    findElementById(id) {
        return this.page.elements.find(element => {
            return element.getId() === id
        })
    }

    /**
     * @return {object}
     */
    getDetailsRequestParams() {
        return {}
    }
}
