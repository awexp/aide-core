const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const path = require('path')

module.exports = {
    mode: 'production',
    output: {
        filename: 'aide.bundle.js',
        path: path.resolve('dist'),
        publicPath: '/'
    },

    resolve: {
        alias: {
            '@core': path.resolve('./lib/core'),
            '@components': path.resolve('./lib/components'),
            '@pages': path.resolve('./lib/pages'),
            '@services': path.resolve('./lib/services'),
            '@store': path.resolve('./lib/store'),
            '@utils': path.resolve('./lib/utils'),
            '@styles': path.resolve('./assets/scss'),
        },

        extensions: ['.js', '.json'],
    },

    module: {
        rules: [
            // note: CSS rules are added by the environment configuration file.

            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            babelrc: true,
                        },
                    },
                ]
            },
        ],
    },

    plugins: [
        new CleanWebpackPlugin([ 'dist' ]),
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: true
        }),
    ],
}