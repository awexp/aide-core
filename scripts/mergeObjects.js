
/**
 * Merges two objects together.
 *
 * @param a
 * @param b
 *
 * @return {object}
 */
module.exports = function mergeObjects(a, b) {
    let result = {}

    const isObject = x => typeof x === 'object' && !Array.isArray(x)
    const transform = (x, y) => {
        if (Array.isArray(x) && Array.isArray(y)) {
            return x.concat(y)
        }

        if (isObject(x) && isObject(y)) {
            return mergeObjects(x, y)
        }

        return y
    }

    for (const [k, v] of Object.entries(a)) { result[k] = v }
    for (const [k, v] of Object.entries(b)) { result[k] = transform(result[k], v) }

    return result
}
