const express = require('express')
const webpack = require('webpack')

const baseWebpackConfig = require('../webpack.config.js')
const envWebpackConfig = require('../env-dev.js')

const mergeObjects = require('./mergeObjects')

const webpackConfig = mergeObjects(
    baseWebpackConfig,
    envWebpackConfig
)

const app = express()
const compiler = webpack(webpackConfig)

compiler.hooks.beforeCompile.tap('x', () => {
    console.clear()
    console.log('Compiling...')
})

app.use(require('webpack-hot-middleware')(compiler));
app.use(require('webpack-dev-middleware')(compiler, {
    publicPath: webpackConfig.output.publicPath,
    hot: true,
    historyApiFallback: true,
    stats: {
        all: false,
        colors: true,
        errors: true,
    }
}))

app.get('/test-project.bundle.js', (req, res) => {
    res.sendFile('C:\\projects\\aide-test-project\\dist\\test-project.bundle.js')
})

app.listen(3000, function () {
    console.log('Listening on port 3000.');
})