const webpack = require('webpack')

const baseWebpackConfig = require('../webpack.config.js')
const envWebpackConfig = require('../env-production.js')

const mergeObjects = require('./mergeObjects')

const webpackConfig = mergeObjects(
    baseWebpackConfig,
    envWebpackConfig
)

webpack(webpackConfig, (err, stats) => {
    console.log('Done.')
})
